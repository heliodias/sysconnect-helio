<?php
    $title = 'Contato | Sysconnect';
    include '_header.php';
?>
<main>
    <section class="cabecalho cabecalho--contato">
        <div class="cabecalho__wrapper">
            <nav class="cabecalho__wrapper__breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a href="./">Home</a></li>
                    <li>Contato</li>
                </ul>
            </nav>
            <div class="cabecalho__wrapper__title">
                <h1>Contato</h1>
            </div><!-- /TITLE -->
        </div> <!-- /WRAPPER -->
    </section>
    <div class="contato">
        <?php 
            if(!isset($_POST['enviar'])):
        ?>
        <section class="contato__form">
            <div class="faleConosco">
                <h2>Fale Conosco</h2>
                <p class="faleConosco__desc">
                    Preencha o <span>formulário</span>
                   <!-- ao lado/abaixo --> que em breve 
                    entraremos em contato.
                </p>
            </div>
            <form action="" method="post">
                <div class="form-row">
                    <div>
                        <label for="nome">Seu nome:</label>
                        <input type="text" name="nome" id="nome" maxlength="50" minlength="8" placeholder="Ex: José Silva" required>
                    </div>
                </div>
                <div class="form-row">
                    <div>
                        <label for="email">Seu e-mail:</label>
                        <input type="email" name="email" id="email" maxlength="50" minlength="8" placeholder="Ex: mail@exemplo.com.br" required>
                    </div>
                    <div>
                        <label for="telefone">Telefone:</label>
                        <input type="tel" name="telefone" id="telefone" minlength="0" maxlength="12" placeholder="Ex: 13 997586542">
                    </div>
                </div>
                <div class="form-row">
                    <div>
                        <label for="assunto">Assunto:</label>
                        <input type="text" name="assunto" id="assunto" maxlength="40" minlength="5" placeholder="Ex: Criação de Sites" required>
                    </div>
                </div>
                <div class="form-row">
                    <div>
                        <label for="mensagem">Mensagem:</label>
                        <textarea name="mensagem" id="mensagem" minlength="50" maxlength="350" cols="30" rows="4" placeholder="Digite aqui a mensagem que deseja nos passar" required></textarea>
                    </div>
                </div>
                <button class="formBtn" name="enviar" id="enviar" value="enviado" type="submit">Enviar</button>
            </form>
            <?php else: ?>
                <div class="contato__enviado">
                    <p class="contato__enviado__ola">
                        Olá <?=$_POST['nome']?>,
                    </p>
                    <p class="contato__enviado__corpo">
                       <span>Recebemos o seu contato sobre <strong><?=$_POST['assunto']?></strong>.</span>
                        Assim que possível retornaremos através do email: <?=$_POST['email']?> ou pelo telefone <?=$_POST['telefone']?>.
                        Se desejar entre em contato conosco pelos nossos outros contatos abaixo, ou conheça todos os nossos serviços:
                    </p>
                    <a href="./servicos.php" class="btnPadrao btnPadrao--branco">Nossos serviços</a>
                </div>
            <?php endif ?>
        </section>
        <section class="contato__vantagens">
            <p class="subtitulo">
                Se desejar nos contate também em
            </p>
            <h2>Nossos outros contatos</h2>
            <div class="contato__vantagens__dados">
                <div class="info">
                    <div class="info__circle info__circle--wpp">
                        <img src="./assets/wpp-ico.png" alt="ícone whatsapp">
                    </div>
                    <p class="info__desc">
                        <span>Whatsapp:</span>
                        
                        <a href="https://api.whatsapp.com/send?phone=5513999999999&text=Vim%20pelo%20site%20da%20Sysconnect" target="_bank">(13) 9 999-9999</a>
                    </p>
                </div>
                <div class="info">
                    <div class="info__circle info__circle--tel">
                        <img src="./assets/tel-ico.png" alt="ícone aparelho telefônico">
                    </div>
                    <p class="info__desc">
                        <span>Telefone:</span>
                        <a href="tel:+551333333333" target="_bank">(13) 3333-3333</a>
                    </p>
                </div>
                <div class="info">
                    <div class="info__circle info__circle--email">
                        <img src="./assets/email-ico.png" alt="ícone envelope de carta">
                    </div>
                    <p class="info__desc">
                        <span>Email:</span>
                       <a href="mailto:contato@sysconnect.com.br" target="_blank">contato@sysconnect.com.br</a>
                    </p>
                </div>
            </div>
        </section>
        <section class="contato__localizacao">
            <h2>Onde nos encontrar</h2>
            <div class="contato__localizacao__mapa">
                <div class="cardLocal">
                    <a href="https://www.google.com/maps?ll=-23.952167,-46.330414&z=16&t=m&hl=pt-BR&gl=BR&mapclient=embed&q=Av.+Senador+Feij%C3%B3,+686+-+Vila+Matias+Santos+-+SP+11015-504" target="_blank">
                        <img src="./assets/localizacao.png" alt="Ícone localização">
                    </a>
                    <div>
                        <h3>Endereço</h3>
                        <address>
                            <a href="https://www.google.com/maps?ll=-23.952167,-46.330414&z=16&t=m&hl=pt-BR&gl=BR&mapclient=embed&q=Av.+Senador+Feij%C3%B3,+686+-+Vila+Matias+Santos+-+SP+11015-504" target="_blank">
                                <span>Av. Senador Feijó, 686 - Sala 1625</span>
                                <span>Vila Mathias - Santos / SP</span>
                                <span>CEP: 11015-504</span>
                            </a>
                        </address>
                    </div>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3646.234608717527!2d-46.32996308558576!3d-23.95214268202304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce03745d931789%3A0xe39d01e5e54271ad!2sAv.%20Senador%20Feij%C3%B3%2C%20686%20-%20Vila%20Matias%2C%20Santos%20-%20SP%2C%2011015-504!5e0!3m2!1spt-BR!2sbr!4v1635194624750!5m2!1spt-BR!2sbr" width="1920" height="800" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
        </section>
    </div>
    <section class="rodape rodape--qmSomos">
        <div class="rodape__wrapper">
            <h2>
                A solução ideal você encontra aqui!
            </h2>
            <a href="./servicos.php" class="btnPadrao btnPadrao--branco">Conheça nossos serviços</a>
        </div>
    </section>
</main>
<?php
    include '_footer.php';
?>