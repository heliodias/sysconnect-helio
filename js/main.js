document.querySelector('.toggler').addEventListener('click', ()=>{
    document.querySelector('.header__menu__list').classList.toggle('active')
    document.querySelector('.fa-bars').classList.toggle('icon-active')
})

//Swiper em Banner
const swiperBanner = new Swiper('.banner .swiper', {
  direction: 'horizontal',
    loop: true,
    
    
    pagination: {
         el: '.swiper-pagination',
         clickable: true,
     },
    });
//Swiper em Destaque
const swiperDestaque = new Swiper('.destaque__container .swiper', {
    direction: 'horizontal',
    loop: true,
    breakpoints: {
      320: {
          slidesPerView: 1,
        },
        800: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        1360: {
          slidesPerView: 3,
        }
      },
      navigation: {
        nextEl: '.dest-next',
        prevEl: '.dest-prev',
      },
      
    });
    //Swiper em Certificações
const swiperCertificacoes = new Swiper('.certificacoes .swiper', {
  direction: 'horizontal',
    loop: true,
    breakpoints: {
      320: {
          slidesPerView: 1,
        },
        800: {
          slidesPerView: 2,
          spaceBetween: 10,
        },
        1360: {
          slidesPerView: 6,
        }
      },
    navigation: {
      nextEl: '.cert-next',
        prevEl: '.cert-prev',
        spaceBetween: 900,
      },

    });
    //Página de contato
    if(window.location.pathname==='/contato.php'){
      document.getElementById('telefone').addEventListener('keyup', e => {
        if(e.target.value.length==2){
            e.target.value+=' '
        }
      })
    }