<?php
    class Servico{
        public $nome;
        public $descricao;
        public $descricaoLong;
        //B.E.M modifier
        public $modifier;
        public function __construct($nome, $descricao, $descricaoLong, $modifier){
            $this->nome = $nome;
            $this->descricao = $descricao;
            $this->descricaoLong = $descricaoLong;
            $this->modifier = $modifier;
        }
    }
    $servicosLista = array(
        new Servico(
        /*nome*/'Suporte e Manutenção',
        /*descricao*/'Atendimento técnico especializado ágil e eficaz. Auxílio também para alimentação de informações.',
        /*descricaoLong*/[
            'Oferecemos <strong>manutenção de sites</strong> de todos os tipos, com atendimento técnico especializado, ágil e eficaz.',
            'A melhor maneira de obter sucesso na web é ter um projeto evolutivo e programado. Justamente em função disso, é muito útil o acompanhamento permanente de técnicos especializados.',
            'Nosso pacote de manutenção de sites básico conta com uma gama de 10 serviços, que vão desde a aprimoramentos de design, atendimento a emergências, aperfeiçoamentos tecnológicos, criação de novos serviços aos usuários, atendimento de chamados para correções, atualizações e adaptações.',
            'Caso seja interessante, podemos acrescentar também aos serviços de manutenção de sites o auxílio para alimentação de fotos, vídeos, textos e artes gráficas, assim você não precisa se preocupa com nada.',
            'O serviço de manutenção permanente de seu site evita a obsolescência e otimiza o seu investimento. Site sempre moderno e bem atualizado. <a href="./contato.php">Entre em contato conosco</a>!',
        ],
        /*modifier*/'suporte'
        ),
        new Servico(
        /*nome*/'Criação de sites',
        /*descricao*/'Sites responsivos, com navegação intuitiva e design profissional. Sites com foco em geração de resultados.',
        /*descricaoLong*/[
            'A <strong>criação de sites</strong> pode parecer algo simples, um produto commodity, mas isto é um grande engano. Uma coisa é criar um site institucional comum, que possui baixíssimo volume de acessos, outra coisa é a criação de sites profissionais, com design moderno, tecnologia de ponta e alta performance comercial. Fazer um site liderar é um grande desafio e podemos te ajudar!',
            'Se não sabe como criar um site que faça a diferença, que se destaque no Google, que entregue grande volume de acessos, vendas, consultas e leads, então você deve nos consultar.',
            'A Sysconnect realiza a criação de sites responsivos, totalmente adaptados a smartphones e tablets. Isso é muito importante pois atualmente a maior parte dos acessos ocorre através de dispositivos móveis. Um site profissional precisa estar adaptado a essa realidade.',
            "Já no que se refere à usabilidade, realizamos a produção de sites com navegação intuitiva e com forte utilização de chamadas para ação ( CTA's ), com foco em resultados comerciais. Realizamos testes A/B para entender o comportamento dos visitantes e com isso aumentamos a sua conversão.",
            '<a href="./servico-descricao.php?name=seo">Sites otimizados para o Google ( SEO )</a>, com as <a href="./servico-descricao.php?name=googleads">palavras-chave</a> ideais para alavancar os seus acessos orgânicos. Isso faz com que seu site tenha uma posição de destaque na 1a página de busca e diminua a necessidade de anúncios pagos, através do Google Ads.',
            'Todos os nossos projetos são realizados sob-medida. Antes de iniciarmos um projeto analisamos as suas necessidades e propomos um a criação de um site com as ferramentas e tecnologias certas para alcançar resultados desejados.',
            'Já desenvolvemos mais de <a href="./quem-somos.php">800 projetos</a> sob-medida, com atenção máxima em entregar resultados reais e através de ótimo custo x benefício. <a href="./contato.php">Entre em contato conosco</a>. Você vai se surpreender!',
        ],
        /*modifier*/'site'
        ),
        new Servico(
        /*nome*/'Sistemas em nuvem',
        /*descricao*/'Software acessível pela web, desenvolvido sob-medida, com segurança de datacenter profissional.',
        /*descricaoLong*/[
            'Os <strong>softwares em nuvem</strong> chegaram para ficar. A possibilidade de disponibilizar sistemas via web permite diversos benefícios incríveis, possibilitando mobilidade e muita facilidades de manutenção. <strong>A Sysconnect desenvolve sistemas em nuvem sob-medida</strong> para uma seleta carteira de clientes.',
            'Basta uma conexão com a internet e você acessa seu sistema em nuvem em um hotel, em casa, em uma filial, no seu cliente, em qualquer lugar. O sistema em nuvem pode ser acessado mesmo a partir de um tablet ou celular.' ,
            'A infraestrutura é livre e segura, com uso de <strong>Data Center</strong> de classe mundial, como por exemplo <strong>Amazon (AWS)</strong>. Não precisa instalar nada no seu computador de trabalho, basta um navegador de internet e você tem independência de Sistema Operacional. Mesmo que a sua rede local seja contaminada por vírus, os seus dados ficam seguros e centralizados em nuvem, sem serem afetados.',
            'Não se preocupe mais com rotinas de backup ou qualquer outra preocupação com a infraestrutura, pois o conceito de Cloud Computing chegou para libertar as empresas destas preocupações',
            'Quando houver necessidade de alterar ou atualizar o seu sistema em nuvem, isso ocorrerá apenas nos servidores - com total segurança. Quando a alteração estiver aprovada, entra em produção e você a acessa sem necessidade de re-instalar nada em seu computador.',
            'Sistema em nuvem permite colaboração utilizando workflow. envio de e-mails de alertas automáticos, SMS´s automáticos, equipes de trabalho à distância, interações automatizadas com os seus clientes, permitindo inclusive o acompanhamento das atividades em tempo real, se for de seu interesse.',
            'Temos muita experiência e já desenvolvemos <a href="./quem-somos.php">muitos sistemas em nuvem</a> para clientes de todos os portes. Utilizamos tecnologias de ponta associadas a metodologias ágeis, que conferem grande assertividade em cronogramas. Cumprimos rigorosamente os prazos para o desenvolvimento de softwares em nuvem, sem surpresas desagradáveis.',
            '<a href="./contato.php">Consulte-nos</a> sobre como podemos ajudá-lo com a criação de um sistema em nuvem para a sua empresa.',
        ],
        /*modifier*/'nuvem'
        ),
        new Servico(
            /*nome*/'E-commerce',
            /*descricao*/'Criação de lojas virtuais com gestão de pedidos, integração com cartões de crédito, boletos, etc.',
            /*descricaoLong*/[
                '<strong>Criação de loja virtual</strong> com alta performance, com gestão de pedidos, integração com cartões de crédito, boletos, etc.',
                'Oferecemos lojas virtuais completas, que possuem interface rica e amigável, com ótimo gerenciamento de produtos, clientes e vendas. Possuem ferramentas flexíveis para que você possa criar diversas promoções e sistemas para fidelização de clientes.',
                'Nosso e-commerce já conta com uma vasta gama de formas de pagamentos e permite também customização de design, porém, dentro de alguns parâmetros. O sistema administrativo da loja virtual oferece relatórios estatísticos completos, para que você possa acompanhar de perto o andamento do seu negócio.',
                'Oferecemos ainda consultoria em e-commerce para que possa dar os primeiros passos com assertividade, economizando recursos na etapa de criação da loja virtual.',
                'Agende uma apresentação, <a href="./contato.php">faça uma consulta</a> e venha tomar um café conosco.',
            ],
            /*modifier*/'ecommerce'
        ),
        new Servico(
            /*nome*/'SEO',
            /*descricao*/'Otimização de sites para destaque no Google. Resultados orgânicos, gerando tráfego e autoridade para o seu site.',
            /*descricaoLong*/[
                'A cada dia que passa a otimização de sites vem se tornando mais importante para qualquer negócio. Se existe algo que 100% das empresas desejam é que o site seja um instrumento de vendas ativo. Na verdade ele pode ser sim e a palavra mágica para isso é: <strong>destaque no Google</strong>.',
                'Se um site está bem otimizado no Google, posicionado na primeira página, então ele está visível àqueles que procuram seus produtos ou serviços.',
                '<strong>Otimização de sites</strong> para o Google é uma arte que depende de um especialista em SEO. Este não é um trabalho para amador. Atualmente o buscador leva em consideração aproximadamente 200 critérios, que vão desde a velocidade do site, a forma como o código do site foi escrito, se o site é responsivo ou não, se o conteúdo é rico, se o site utiliza dados estruturados, se o site conta com links importantes apontando para ele ( linkbuiding ), se o domínio possui boa autoridade, se o código foi escrito utilizando as tags corretas, se existe ou não a presença de um sitemap, entre muitos outros fatores.',
                'O fato é que ter um site otimizado é um diferencial importante de mercado, pois o número de visitantes aumenta exponencialmente e com isso, o número de vendas e contatos gerados a partir do site. Em nossos projetos de otimização de sites acompanhamos de perto a evolução dos acessos, a evolução de conversões do site e diversas outras métricas importantes para o seu negócio.',
                'Técnicas bem aplicadas de otimização de sites (SEO) são essenciais para gerar resultados em tráfego orgânico - sem que sejam necessários anúncios pagos. Isso faz com que a autoridade do site aumente dia após dia e os resultados do site no Google se tornam ainda melhores.',
                'Potencialize as oportunidades comerciais que a internet pode gerar ao seu negócio com a otimização de sites para o Google (SEO). <a href="./contato.php">Consulte-nos!</a>'
            ],
            /*modifier*/'seo'
        ),
        new Servico(
            /*nome*/'Intranet e Extranet',
            /*descricao*/ 'Equipe integrada em um único ambiente, controle de processos e maior produtividade.',
            /*descricaoLong*/[
                'Que tal ter uma <strong>intranet ou extranet</strong> em sua empresa? As vantagens são diversas, tais como: ter equipe integrada em um único ambiente, distribuição de informações facilitada, controle de processos e maior produtividade.',
                'A Sysconnect <a href="./quem-somos.php">desenvolve intranets e extranets com projetos desenvolvidos sob-medida</a>, de acordo com o seu negócio. É possível customização das ferramentas, e escolher de forma modular aquelas que são mais importantes para a sua empresa.',
                '<ul>
                    <li>Compartilhamento de informações e documentos</li>
                    <li>Redução no uso de papel</li>
                    <li>Facilidade de acesso à informação</li>
                    <li>Controle da produtividade</li>
                    <li>Interatividade entre funcionários</li>
                    <li>Difusão da cultura da sua empresa</li>
                    <li>Trabalho colaborativo</li>
                </ul>',
                'Podemos oferecer uma intranet corporativa flexível e muito simples de ser usada.',
                '<a href="./contato.php">Entre em contato conosco</a>!'
            ],
            /*modifier*/'intranet'
        ),
        new Servico(
            /*nome*/'Google Adwords',
            /*descricao*/ 'Campanhas Google Ads, planejamento estratégico e análise de palavras-chave. Relatório mensal de performance.',
            /*descricaoLong*/[
                'Alguns setores comerciais são muito disputados no Google. Nestes casos, o ideal é pensar em dar um “empurrãozinho” no seu posicionamento através dos links patrocinados ( <strong>Google Adwords</strong> ).',
                'A primeira coisa que passa pela cabeça das pessoas é: vou gastar uma fortuna com anúncio no google para ficar na 1ª página, mas isso não é verdade.',
                'Uma campanha <strong>Google Ads</strong> bem planejada é mais barata do que imagina. Você precisa contar com especialistas google adwords para gerir de modo profissional a sua campanha. A Sysconnect pode encontrar nichos de mercado a serem explorados, palavras-chave com alto potencial e menor concorrência, combinar termos específicos ( long tail ) com localizações geográficas e características do seu público alvo. Entregamos bons resultados com baixo orçamento.',
                '<a href="./contato.php">Entre em contato conosco</a>. Podemos ajudá-lo a encontrar a melhor relação custo x benefício nos anúncios Google Adwords.'
            ],
            /*modifier*/'googleads'
        ),
        new Servico(
            /*nome*/"Criação de App's",
            /*descricao*/ 'Criação de aplicativos para Iphone e Android. Desenvolvimento de aplicativos sob medida.',
            /*descricaoLong*/[
                'Atualmente as pessoas não desgrudam de seus celulares, não é mesmo? Os smartphones funcionam como uma central de entretenimento para uso pessoal e um verdadeiro computador de bolso para o mundo do trabalho. Goste ou não, o smartphone faz parte do cotidiano de todos nós e a criação de aplicativos pode sim ajudar nos seus negócios.',
                'Que tal oferecer aos seus clientes um <strong>aplicativo moderno e prático</strong> para facilitar a vida deles?',
                'A Sysconnect oferece serviço de <strong>criação de aplicativos diferenciados para a plataforma Iphone (IOS) e Android</strong> e é mais barato e fácil do que você imagina.',
                'As empresas que puderem criar apps e oferecer conveniência aos seus clientes, com certeza terão um grande diferencial competitivo. Permitir consulta de informações, agilizar um processo que depende de diversos passos, facilitar o processo de comunicação, entre muitas outras facilidades.',
                'A criação de aplicativos na Sysconnect utiliza Node JS e React - uma plataforma de desenvolvimento de aplicativos moderna e especialmente interessante a quem deseja criar apps para os dois sistemas ( IOS e Android ). O desenvolvimento de aplicativos é feito sob medida, deste modo entendemos sua necessidade e propomos a criação de aplicativos personalizados, com design exclusivo e plenamente de acordo com as suas necessidades.',
                'Saiba como dar os primeiros passos para criar um aplicativo. Consulte a Sysconnect!'
            ],
            /*modifier*/'apps'
        ),
        new Servico(
            /*nome*/'Marketing de Conteúdo',
            /*descricao*/ 'Produção de conteúdo para Blogs, com foco em atração de visitantes através do Google. Posts 100% otimizados.',
            /*descricaoLong*/[
                '<strong>Marketing de Conteúdo</strong> pode ser entendido como a produção de informações relevantes objetivando trazer o público-alvo desejado ao seu site. Trata-se de uma forma de atrair e envolver as pessoas certas, entregando informações importantes para que decidam comprar o seu produto ou serviço.',
                "Mas não basta escrever um tratado sobre os seus produtos e disponibilizar na web. Na verdade, em Marketing de Conteúdo a forma como o texto é escrito e a <a href='./servico-descricao.php?name=googleads'>escolha das palavras-chaves</a> corretas podem ser até mais importantes do que o volume de informações em si. É necessário conhecer todas as técnicas de otimização on-page e é preciso entender como fisgar o visitante do site com CTA's ( chamadas para ação ) realmente eficientes.",
                'Conte com a nossa experiência para planejar uma campanha de marketing de conteúdo vencedora, criando personas, as pautas, produzindo conteúdos de alta performance para o seu blog e apresentando os resultados das ações.',
                'Um post bem posicionado no Google pode significar bons negócios por muito tempo e o que é melhor: uma vez que ele esteja no topo das buscas não é necessário gastar mais nenhum centavo. Ele se transforma em uma máquina de fazer negócios. Pense nisso.',
                'Temos uma equipe especializada em Marketing de Conteúdo para entregar os melhores resultados à sua empresa. Faça uma consulta!'
            ],
            /*modifier*/'mktconteudo'
        ),
    )
?>