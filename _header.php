<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="scss/index.css">
    <link
      rel="stylesheet"
        href="https://unpkg.com/swiper@7/swiper-bundle.min.css"
    />
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
    <meta name="format-detection" content="telephone=no">
    <script src="js/main.js" defer></script>
</head>
<body>
    <!-- class "header" redundante para uso do B.E.M no SASS -->
    <header class="header">
        <div class="header__social">
            <ul>
                <li><a href="https://www.linkedin.com/company/kbr-tec---solu-es-on-line-ltda/mycompany/" target="_blank"><img src="assets/linkedin.png" alt="Logo do Linkedin"></a></li>
                <li><a href="https://twitter.com/kbrtec" target="_blank"><img src="assets/twitter.png" alt="Logo do Twitter"></a></li>
                <li><a href="https://www.facebook.com/kbrtec" target="_blank"><img src="assets/facebook.png" alt="Logo do Facebook"></a></li>
                <li><a href="https://www.youtube.com/channel/UCyUfrnAuRCD6LGHtSxF2qkw" target="_blank"><img src="assets/youtube.png" alt="Logo do Youtube"></a></li> 
            </ul>
        </div>
        <div class="wrapper">
            <nav class="header__menu">  
                <a href="./">
                    <img src="assets/logo-text.png" alt="Logo da Sysconnect com símbolo e tipografia">
                </a>
                <button class="toggler">
                    <i class="fa fa-bars"></i>  
                </button>
                <ul class="header__menu__list">
                    <li><a href="./quem-somos.php">Quem somos</a></li>
                    <li><a href="./servicos.php">Serviços</a></li>
                    <li><a href="./blog.php">Blog</a></li>
                    <li><a href="./contato.php">Contato</a></li>
                </ul>
            </nav>
        </div>
    </header>