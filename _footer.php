<!-- class "footer" redundante para uso do B.E.M no SASS -->
<footer class="footer">
        <!--Div sem classe como flex-container-->
        <div>
            <div class="footer__menu">
                <ul>
                    <li><a href="./quem-somos.php" class="footer__menu__destaque">Quem Somos</a></li>
                    <li><a href="./servicos.php" class="footer__menu__destaque">Serviços</a></li>
                    <?php
                        include_once '_servicosArr.php';
                        foreach($servicosLista as $servico):
                    ?>
                        <li><a href="servico-descricao.php?name=<?= $servico->modifier ?>"><?=$servico->nome?></a></li>
                     <?php 
                        endforeach;
                    ?>      
                </ul>
                <ul>
                    <li><a href="./blog.php" class="footer__menu__destaque">Blog</a></li>
                    <li><a href="./contato.php" class="footer__menu__destaque">Contato</a></li>
                </ul>
            </div>
            <div class="footer__contato">
                <h2>
                    Siga-nos:
                </h2>
                <ul class="footer__contato__social">
                    <li><a href="https://www.linkedin.com/company/kbr-tec---solu-es-on-line-ltda/mycompany/" target="_blank"><img src="assets/linkedin-blue.png" alt="Logo do Linkedin"></a></li>
                    <li><a href="https://twitter.com/kbrtec" target="_blank"><img src="assets/twitter-blue.png" alt="Logo do Twitter"></a></li>
                    <li><a href="https://www.facebook.com/kbrtec" target="_blank"><img src="assets/facebook-blue.png" alt="Logo do Facebook"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCyUfrnAuRCD6LGHtSxF2qkw" target="_blank"><img src="assets/youtube-blue.png" alt="Logo do Youtube"></a></li> 
                </ul>
                <h2>Telefone</h2>
                <a href="tel:+550012345675" target="_blank">(00) 1234-5675</a>
                <h2>E-mail</h2>
                <a href="mailto:contato@sysconnect.com" target="_blank">contato@sysconnect.com</a>
            </div>
        </div>
        <div class="footer__copyright">
            <span class="footer__copyright__wrapper">
                <p>
                    2021 - Todos os direitos reservados
                </p>
                <a href="https://www.kbrtec.com.br/" target="_blank">
                    Desenvolvido por <img src="./assets/kbrtec-logo.png" alt="Logo KBRTEC">
                </a>
            </span>
        </div>
    </footer>
</body>
</html>
