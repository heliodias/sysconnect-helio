<?php
    $title = 'Sysconnect - Especialista em Sistemas Web';
    include '_header.php';
?>
<main>

        <!-- Banner-->

        <div class="banner">
            <!-- Foge do B.E.M por ser parte da lib -->
            <div class="swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <section class="banner__especialista">
                            <h1>
                                A Sysconnect é especialista em sistemas web
                            </h1>
                            <p>
                                Entre em contato conosco e conheça nossas soluções personalizavéis para cada negócio.
                            </p>
                            <a href="./contato.php" class="btnBanner">
                                Saiba mais
                            </a>
                        </section>
                    </div>
                    <div class="swiper-slide">
                        <section class="banner__especialista">
                            <h1>
                                A Sysconnect é especialista em sistemas web
                            </h1>
                            <p>
                                Entre em contato conosco e conheça nossas soluções personalizavéis para cada negócio.
                            </p>
                            <a href="./contato.php" class="btnBanner">
                                Saiba mais
                            </a>
                        </section>
                    </div>
                    <div class="swiper-slide">
                        <section class="banner__especialista">
                            <h1>
                                A Sysconnect é especialista em sistemas web
                            </h1>
                            <p>
                                Entre em contato conosco e conheça nossas soluções personalizavéis para cada negócio.
                            </p>
                            <a href="./contato.php" class="btnBanner">
                                Saiba mais
                            </a>
                        </section>
                    </div>
                    
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>

        <!-- Destaque -->

        <div class="destaque">
            <!-- Foge do B.E.M por ser parte da lib -->
            <div class="destaque__container">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <?php
                            include_once '_servicosArr.php';
                            foreach($servicosLista as $servico):
                        ?>
                        <div class="swiper-slide">
                            <div class="servicos__card">
                                <h3>
                                    <?= $servico->nome ?>
                                </h3>
                                <p>
                                    <?= $servico->descricao ?>
                                </p>
                                <a class="lerMais" href="servico-descricao.php?name=<?= $servico->modifier ?>">&gt; saiba mais</a>
                                <div class="servicos__card__img servicos__card__img--<?= $servico->modifier?>"></div>
                            </div> <!--/CARD-->
                        </div> <!--/SLIDE-->
                        <?php 
                            endforeach;
                        ?>
                    </div><!--/WRAPPER-->
                </div> <!--/SWIPPER-->
                <div class="swiper-button-prev dest-prev"></div>
                <div class="swiper-button-next dest-next"></div>
            </div> <!--/DESTAQUE-CONTAINER-->
        </div> <!--/DESTAQUE -->

        <!-- Certificações -->

        <section class="certificacoes">
            <h2>Conheça nossos clientes</h2>
            <div class="certificacoes__container">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <a href="https://www.samsung.com/br/" target="_blank">
                                <img src="./assets/samsung.png " alt="Logo Samsung">
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="https://www.kbrtec.com.br/" target="_blank">
                                <img src="./assets/kbrtec.png " alt="Logo KBRTEC">
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="https://www.capital.sp.gov.br/" target="_blank">
                                <img src="./assets/prefeitura-sp.png " alt="Brasão Prefeitura de São Paulo">
                            </a>
                        </div>  
                        <div class="swiper-slide">
                            <a href="https://www.philips.com.br/"  target="_blank">
                                <img src="./assets/philips.png " alt="Logo Philips">
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="https://www.mktvirtual.com.br/" target="_blank">
                                <img src="./assets/mktvirtual.png " alt="Logo Marketing Virtual">
                            </a>
                        </div>
                        <div class="swiper-slide">
                            <a href="https://www.itautec.com/" target="_blank">
                                <img src="./assets/itautec.png " alt="Logo Itautec">
                            </a>    
                        </div>
                    </div><!-- /WRAPPER -->

                </div><!--/swiper -->
                <div class="swiper-button-prev cert-prev"></div>
                <div class="swiper-button-next cert-next"></div>
            </div>
        </section>
        <div class="blogContainer">
            <section class="blogContainer__downloads">
                <h2>
                    <a href="./quem-somos.php">
                        A Sysconnect
                    </a>
                </h2>
                <a href="./quem-somos.php">
                    <img src="assets/downloads-1.png" alt="Pessoa tecla em notebook">
                </a>
                <p>
                    <a href="./quem-somos.php">
                        Lorem ipsum dolor sit amet, consectetur elit ornare
                        tempus duis ornare elit vitae velit tempus, nec...
                    </a>
            </p>
            </section>
            <section class="blogContainer__noticias">
                <h2>
                    <a href="./blog.php">
                        Blog
                    </a>
                </h2>
                <div class="blogContainer__noticias__wrapper">
                    <section class="blogContainer__noticias__wrapper__destacada">
                        <a href="./blog.php" class="destaque-blog">
                            <img src="assets/blog-1.png" alt="Homem de óculos segura notebook">
                        </a>
                        <h3>
                            <a href="./blog.php">
                                Destaques
                            </a>
                        </h3>
                        <?php 
                            include '_blogPosts.php';
                        ?>
                        <p>
                            <a href="./blog-post.php?id=1">
                                Lorem ipsum dolor sit amet, consectetur elit ornare
                                tempus duis ornare elit vitae velit tempus, nec.
                            </a>
                        </p>
                        <a href="./blog-post.php?id=1">&gt; ler mais</a>
                    </section>
                    <div class="blogContainer__noticias__wrapper__outras">
                        <div>
                            <p>
                                <a href="./blog-post.php?id=2">
                                    Lorem ipsum dolor sit amet, consectetur elit ornare
                                    tempus duis ornare elit vitae velit tempus, nec...
                                </a>
                            </p>
                            <a href="./blog-post.php?id=2">&gt; ler mais</a>
                        </div>
                        <div>
                            <p>
                                <a href="./blog-post.php?id=3">
                                    Lorem ipsum dolor sit amet, consectetur elit ornare
                                    tempus duis ornare elit vitae velit tempus, nec...
                                </a>
                            </p>
                            <a href="./blog-post.php?id=3">&gt; ler mais</a>
                        </div>
                        <div>
                            <p>
                                <a href="./blog-post.php?id=4">
                                    Lorem ipsum dolor sit amet, consectetur elit ornare
                                    tempus duis ornare elit vitae velit tempus, nec...
                                </a>
                            </p>
                            <a href="./blog-post.php?id=4">&gt; ler mais</a>
                        </div> <!-- /DIV OUTRA NOTÍCIA -->
                    </div> <!-- /OUTRAS -->
                </div> <!-- /CONTAINER -->
            </section>
        </div>
    </main>
    <?php 
        include '_footer.php';
    ?>