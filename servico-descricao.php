<?php
    include_once '_servicosArr.php';
    if(isset($_GET['name'])){
        $exists = false;
        foreach($servicosLista as $servico){
            if($servico->modifier==$_GET['name']){
                $exists = true;
                $servicoPG = $servico;
            }
        }
        if(!$exists){
            header('Location: servicos.php');
        }
    }else{
        header('Location: servicos.php');
    }
?>
<?php
    $title = $servicoPG->nome . ' | Serviços | Sysconnect';
    include '_header.php';
?>
<main>
    <section class="cabecalho cabecalho--servicoIntegra cabecalho--<?php echo $servicoPG->modifier ?>">
        <div class="cabecalho__wrapper">
            <nav class="cabecalho__wrapper__breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a href="./">Home</a></li>
                    <li><a href="./servicos.php">Serviços</a></li>
                </ul>
            </nav>
            <div class="cabecalho__wrapper__title">
                <h1>
                    <?php echo $servicoPG->nome ?>
                </h1>
            </div><!-- /TITLE -->
        </div> <!-- /WRAPPER -->
    </section>
    <div class="servicoIntegra">
        <div class="descriptionCard descriptionCard--<?php echo $servicoPG->modifier ?>">
            <div class="descriptionCard__texto">
                    <?php foreach($servicoPG->descricaoLong as $descricao): ?>
                        <p>
                            <?= $descricao ?>
                        </p>
                    <?php endforeach ?>
                <a href="./contato.php" class="btnPadrao btnPadrao--azul">Faça já seu orçamento</a>
            </div>
            <div class="descriptionCard__img">
            </div>
        </div>
        <section class="destaque">
            <p class="subtitulo">Gostou? Conheça mais os nossos</p>
            <h2>Outros serviços</h2>
        
            <!-- Foge do B.E.M por ser parte da lib -->
            <div class="destaque__container">
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <?php
                            include_once '_servicosArr.php';
                            foreach($servicosLista as $servico):
                        ?>
                        <?php if($servico->nome!==$servicoPG->nome):?>
                        <div class="swiper-slide">
                            <div class="servicos__card">
                                <h3>
                                    <?= $servico->nome ?>
                                </h3>
                                <p>
                                    <?= $servico->descricao ?>
                                </p>
                                <a class="lerMais" href="servico-descricao.php?name=<?= $servico->modifier ?>">&gt; saiba mais</a>
                                <div class="servicos__card__img servicos__card__img--<?= $servico->modifier?>"></div>
                            </div> <!--/CARD-->
                        </div> <!--/SLIDE-->
                        <?php endif ?>
                        <?php 
                            endforeach;
                        ?>
                    </div><!--/WRAPPER-->
                </div> <!--/SWIPPER-->
                <div class="swiper-button-prev dest-prev"></div>
                <div class="swiper-button-next dest-next"></div>
            </div> <!--/DESTAQUE-CONTAINER-->
        </section> <!--/DESTAQUE -->
    </div>
    <section class="rodape rodape--servicoIntegra">
        <div class="rodape__wrapper">
            <h2>
                A solução ideal você encontra aqui!
            </h2>
            <a href="./contato.php" class="btnPadrao btnPadrao--branco">Quero um Orçamento Gratuito</a>
        </div>
    </section>
</main>
<?php
    include '_footer.php';
?>