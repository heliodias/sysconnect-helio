<?php 
    $title = 'Quem somos | Sysconnect';
    include '_header.php';
?>
<main>
    <section class="cabecalho cabecalho--qmSomos">
        <div class="cabecalho__wrapper">
            <nav class="cabecalho__wrapper__breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a href="./">Home</a></li>
                    <li>Quem Somos</li>
                </ul>
            </nav>
            <div class="cabecalho__wrapper__title">
                <h1>Quem Somos</h1>
            </div><!-- /TITLE -->
        </div> <!-- /WRAPPER -->
    </section>
    <section class="sobre">
        <div class="descriptionCard descriptionCard--qmSomos">
            <div class="descriptionCard__texto">
                <p class="descriptionCard__texto__bmVindo">
                    Bem - vindo à nossa empresa
                </p>
                <h2>um resumo Sobre A Sysconnect</h2>
                <p>
                    A Sysconnect nasceu da vontade de fornecer aos seus clientes serviços que possam
                    alavancar seus negócios através da tecnologia, fornecendo soluções que estejam de
                    acordo com aquilo que eles esperam e o que o mercado necessita.
                </p>
                <p>
                    Nosso maior desafio é materializar boas ideias em soluções de sucesso - práticas
                    e simples, com foco total em resultados. Este é o nosso trabalho e esta é a nossa
                    paixão!
                </p>
                <p>
                    Nosso time é muito experiente e 100% focado em entregar ações de grande impacto,
                    com resultados mensuráveis aos nossos clientes. Oferecemos design moderno com 
                    tecnologia de ponta. Atenção total ao seu posicionamento no Google e performance
                    mobile (celulares e tablets).
                </p>
                <a href="./contato.php" class="btnPadrao btnPadrao--azul">Dúvida? Fale Conosco</a>
            </div>
            <div class="descriptionCard__img">
            </div>
        </div>
        <section class="sobre__vantagens">
            <p class="sobre__vantagens__pq">
                Porque somos diferentes?
            </p>
            <h2>Nossos resultados são baseados em dados</h2>
            <div class="sobre__vantagens__dados">
                <div class="info">
                    <div class="info__circle">
                        8
                    </div>
                    <h3>
                        Estados Brasileiros
                    </h3>
                </div>
                <div class="info">
                    <div class="info__circle">
                        15
                    </div>
                    <h3>
                        <span>Anos</span>de Mercado
                    </h3>
                </div>
                <div class="info">
                    <div class="info__circle">
                        800
                    </div>
                    <h3>
                        Projetos Desenvolvidos
                    </h3>
                </div>
                <div class="info">
                    <div class="info__circle info__circle--google">

                    </div>
                    <h3>
                        <span>Selo</span>Google Partner
                    </h3>
                </div><!-- /INFO -->
            </div><!--/DADOS -->
        </section><!-- /VANTAGENS -->
    </section> <!-- /SOBRE -->
    <section class="rodape rodape--qmSomos">
        <div class="rodape__wrapper">
            <h2>
                A solução ideal você encontra aqui!
            </h2>
            <a href="./contato.php" class="btnPadrao btnPadrao--branco">Quero um Orçamento Gratuito</a>
        </div>
    </section>

</main>
<?php 
    include '_footer.php';
?>