<?php
    $title = 'Serviços | Sysconnect';
    include '_header.php';
?>
<main>
    <section class="cabecalho cabecalho--servicos">
        <div class="cabecalho__wrapper">
            <nav class="cabecalho__wrapper__breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a href="./">Home</a></li>
                    <li>Servicos</li>
                </ul>
            </nav>
            <div class="cabecalho__wrapper__title">
                <h1>Serviços</h1>
            </div><!-- /TITLE -->
        </div> <!-- /WRAPPER -->
    </section>
    <section class="servicos">
        <p class="servicos__subtitulo">
            Conheça todos os nossos serviços 
        </p>
        <h2>
            Somos especialistas em desenvolvimento de soluções em ti
        </h2>
        <div class="servicos__wrapper">
            <?php
                include_once '_servicosArr.php';
                foreach($servicosLista as $servico):
            ?>
                <div class="servicos__card">
                    <h3>
                        <?= $servico->nome ?>
                    </h3>
                    <p>
                        <?= $servico->descricao ?>
                    </p>
                    <a class="lerMais" href="servico-descricao.php?name=<?= $servico->modifier ?>">&gt; saiba mais</a>
                    <div class="servicos__card__img servicos__card__img--<?= $servico->modifier?>"></div>
                </div> <!--/CARD-->
            <?php 
                endforeach;
            ?>
        </div><!--/WRAPPER-->
    </section>
    <section class="rodape rodape--servicos">
        <div class="rodape__wrapper">
            <h2>
                <span>
                    Gostou dos nossos serviços?
                </span>
                Faça já seu orçamento conosco
            </h2>
            <a href="./contato.php" class="btnPadrao btnPadrao--branco">Quero um Orçamento Gratuito</a>
        </div>
    </section>
</main>
<?php
    include '_footer.php';
?>