<?php
    include_once '_blogPosts.php';
    if(isset($_GET['id'])){
        $exists = false;
        foreach($postLista as $post){
            if($post['id']==$_GET['id']){
                $exists = true;
                $postPG = $post;
            }
        }
        if(!$exists){
            header('Location: blog.php');
        }
    }else{
        header('Location: blog.php');
    }
?>
<?php
    $title = $postPG['htmlTitle'] . ' | Blog | Sysconnect';
    include '_header.php';
?>
<main>
    <section class="cabecalho cabecalho--blogPost">
        <div class="cabecalho__wrapper">
            <nav class="cabecalho__wrapper__breadcrumb">
                <ul>
                    <li>Você está em</li>
                    <li><a href="./">Home</a></li>
                    <li><a href="./blog">Blog</a></li>
                </ul>
            </nav>
            <div class="cabecalho__wrapper__title">
                <h1><?php echo $postPG['titulo'] ?></h1>
            </div><!-- /TITLE -->
        </div> <!-- /WRAPPER -->
    </section>
    <div class="blogIntegra">
        <div class="blog blogIntegra__container">
            <div class="roundedPost">
                <img src="<?php echo $postPG['imgSrc'] ?>" alt="<?php echo $postPG['imgAlt'] ?>">
                <div class="roundedPost__texto">
                    <p>
                        Os diversos aplicativos existentes e as facilidades que eles podem trazer para os usuários, fizeram com que diversos investidores e empreendedores começassem a olhá-los como uma possível fonte de renda, além da criação de uma inovação capaz de diversificar e facilitar a vida das pessoas. Mas quanto custa um aplicativo?
                        Talvez essa seja a primeira pergunta que os futuros criadores fazem quando a ideia surge. Para que essa resposta seja dada de forma assertiva, será necessário entender como funcionam as empresas responsáveis pela criação de app e quais são os processos de produção.
                        Se você tem uma ideia genial e quer colocá-la em prática, você precisa conhecer todos os passos e preços para a realização, por isso o presente artigo visa sanar todas as dúvidas existentes dentro desse universo.
                    </p>
                    <h2>Qualquer pessoa pode criar um App?</h2>
                    <p>
                        Essa pergunta pode ser respondida de forma afirmativa, porém, indiretamente. O desenvolvimento de um aplicativo, dentro de um contexto mais prático, deverá ser executado por um profissional que tenha conhecimento técnico e que já tenha familiaridade em programação e as diversas linguagens existentes, porém a realidade pode ser um pouco diferente, pois a maioria das ideias podem surgir de pessoas com pouco ou nenhum conhecimento nesse tipo de tecnologia.
                        Para exemplificar, podemos pegar um restaurante que queira implantar uma campanha de descontos e deseja que todo o projeto seja desenvolvido e controlado através de um aplicativo. Percebe-se que a ideia inicial veio do dono do estabelecimento, então ele deverá procurar um profissional autônomo ou alguma empresa de criação de APP, para que a execução da nova função seja feita.
                    </p>
                    <h2>
                        Quais são os principais tipos de APP?
                    </h2>
                    <p>
                    Seguindo com a necessidade da contratação de um profissional ou empresa para o desenvolvimento do APP, a escolha entre os tipos existentes também deverá ser feita por quem entende da área. Após a ideia ser concretizada, os técnicos irão fazer uma análise, juntamente com o idealizador, para entender qual será a finalidade do aplicativo e também escolher o seu tipo.
                    Hoje, no mercado, pode-se encontrar três tipos principais, sendo um fator determinante na hora da precificação do mesmo. Veja:
                    </p>
                    <p><strong>- Nativo:</strong>APP Nativo leva vantagem pela sua função de trabalho no modo offline e também pela capacidade de aproveitamento das diversas funções existentes nos celulares e tablets. Eles são instalados, interiormente, nos dispositivos e seus códigos podem ser facilmente modificados. A principal desvantagem são os preços elevados que eles podem ter em relação aos outros tipos.</p>
                    <p><strong>- Web App:</strong> O aplicativo Web App pode ser apresentado como uma página que representa as funções de alguns aplicativos existentes nos dispositivos móveis. Para que o usuário consiga o acesso, basta ele utilizar a URL correta, onde apenas uma simples instalação possa ser pedida. A sua principal vantagem é a facilidade de utilização e também o preço, que apresenta valor abaixo quando comparado os outros tipos.</p>

                    <p><strong>- Híbridos:</strong>Por possuir um desenvolvimento incompleto nas linguagens específicas dos sistemas operacionais, ele pode ser apresentado em dois formatos: uma parte nativa e outra web App, então a sua utilização pode ser feita tanto pelo computador quanto pelo celular ou tablet. Esse tipo é bastante procurado devido a sua dupla função e também pelo baixo custo.</p>
                    <h2>
                        Vantagens de ter um aplicativo para sua empresa
                    </h2>
                    <p>
                        Não é novidade que os celulares e tablets estão sendo amplamente utilizados, tanto pela rapidez quanto pela facilidade de uso em qualquer lugar. Diante desse cenário, as empresas que tomam a consciência de que a sua marca também deve estar presente dentro desse mundo, conseguem se destacar aumentando a sua dinâmica de trabalho e também a sua receita. Veja outros benefícios que um aplicativo pode trazer:
                    </p>
                    <ul>
                        <li>
                            - Aumento da comunicação com os clientes
                        </li>
                        <li>
                            - Facilidade na criação de canais de vendas
                        </li>
                        <li>
                            - Visibilidade da marca
                        </li>
                        <li>
                            - Implementação de campanhas e promoções
                        </li>
                        <li>
                            - Controle de gastos
                        </li>
                        <li>
                            - Criação de canal de dúvidas e sugestões
                        </li>
                    </ul>
                    <h2>Afinal quanto custa um aplicativo?</h2>
                    <p>
                        Para estimar quanto custa um aplicativo, as variáveis apresentadas devem ser consideradas, lembrando que outros fatores também podem ter peso na hora de estipular o valor final. Outro ponto importante ainda não citado e que também tem uma influência grande na hora da precificação é a interface, que dependendo da escolha, pode fazer com que o aplicativo assuma um valor bem maior do que o esperado. Para chegar a um valor médio, os preços que serão apresentados foram separados de acordo com o nível tecnológico utilizado e também o tipo de profissional que irá realizar o trabalho, sendo eles:
                    </p>
                    <ul>
                        <li>
                            <strong>- Entre R$3.500,00 e R$6.000,00:</strong> geralmente o aplicativo não irá utilizar uma tecnologia tão avançada e a sua interface será simples.
                        </li>
                        <li>
                            <strong>- Entre R$7.000,00 e R$15.000,00:</strong> o aplicativo irá apresentar uma qualidade maior e pode ter a finalidade de atender um público volumoso.
                        </li>
                        <li>
                            <strong>- A partir de R$15.000,00: </strong> ele terá que ser desenvolvido por profissional altamente qualificado, com uma interface bastante inovadora e com a finalidade de atrair e atender milhões de usuários.
                        </li>
                    </ul>
                    <p>
                        Agora que você já tem uma ideia quanto custa um aplicativo, pense no modelo de negócio e veja o quão importante será o app para o usuário final. Diante de milhares de opções, se faz necessário que o aplicativo tenha uma função de grande relevância para que o usuário mantenha instalado em seu dispositivo.
                    </p>
                    <div class="roundedPost__texto__share">
                        Compartilhe
                        <div>
                            <a href="#">
                                <img src="./assets/fb-share.png" alt="Compartilhe no facebook">
                            </a>
                            <a href="#">
                                <img src="./assets/twt-share.png" alt="Compartilhe no twitter">
                            </a>
                            <a href="#">
                                <img src="./assets/linkd-share.png" alt="Compartilhe no linkedin">
                            </a>
                            <a href="#">
                                <img src="./assets/wpp-share.png" alt="Compartilhe no whatsapp">
                            </a>
                        </div>
                    </div>
                </div> <!-- /roundedPost__TEXTO -->
            </div> <!-- /roundedPost-->
        </div> <!--/BLOG-->
        <div class="blogIntegra__outrosPosts">
            <p class="subtitulo">Leia Mais</p>
            <h2>Outras notícias</h2>
            <div class="blogIntegra__outrosPosts__wrapper">
                <?php
                    include_once '_blogPosts.php';
                    foreach($postLista as $post):
                ?>
                <?php if($post['titulo']!==$postPG['titulo']):?>
                <div class="cardPost">
                    <img src="<?=$post['imgSrc']?>" alt="<?=$post['imgAlt']?>">
                    <div class="cardPost__texto">
                        <em><?=$post['data']?></em>
                        <h3>
                            <a href="<?=$post['link']?>">
                                <?=$post['titulo']?>
                            </a>
                        </h3>
                        <a class="blogLer" href="<?=$post['link']?>">&gt; leia mais</a>
                    </div>
                </div>
                <?php endif ?>
                <?php endforeach ?>
            </div><!-- /OUTROSPOSTS__WRAPPER -->
            
        </div> <!-- /OUTROSPOSTS -->
    </div><!--/BLOG-INTEGRA-->
    <section class="rodape rodape--blog">
        <div class="rodape__wrapper">
            <h2>
                <span>Buscando Transformar sua ideia em negócio?</span>
                Faça já seu orçamento Conosco
            </h2>
            <a href="./contato.php" class="btnPadrao btnPadrao--branco">Quero um orçamento gratuito</a>
        </div>
    </section>
</main>
<?php
    include '_footer.php';
?>