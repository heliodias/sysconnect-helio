<?php
    $postLista = array(
        array(
            "id"=>1,
            "data"=> "05/10/2021",
            "titulo"=>"<span>Desenvolvimento de APP’s:</span>Quanto custa um aplicativo?",
            "htmlTitle"=>"Desenvolvimento de APP'S: Quanto custa um aplicativo?",
            "imgSrc"=>"./assets/post-id-1.png",
            "imgAlt"=>"Mão feminina desenha wireframe",
            "link"=>"./blog-post.php?id=1",
        ),
        array(
            "id"=>2,
            "data"=>"04/10/2021",
            "titulo"=>"Por que meu blog não consegue aparecer no Google?",
            "htmlTitle"=>"Por que meu blog não consegue aparecer no Google?",
            "imgSrc"=>"./assets/post-id-2.png",
            "imgAlt"=>"Mão segurando smartphone com a página google.com aberta",
            "link"=>"./blog-post.php?id=2",
        ),
        array(
            "id"=>3,
            "data"=>"04/10/2021",
            "titulo"=>"WordPress, Drupal, Shopify, Joomla ou Wix: Qual é a melhor plataforma para criação de conteúdo?",
            "htmlTitle"=>"WordPress, Drupal, Shopify, Joomla ou Wix: Qual é a melhor plataforma para criação de conteúdo?",
            "imgSrc"=>"./assets/post-id-3.png",
            "imgAlt"=>"Notebook aberto na página wordpress.com",
            "link"=>"./blog-post.php?id=3",
        ),
        array(
            "id"=>4,
            "data"=>"03/10/2021",
            "titulo"=>"Responsividade: como e porquê ela é importante para o seu site/portal?",
            "htmlTitle"=>"Responsividade: como e porquê ela é importante para o seu site/portal?",
            "imgSrc"=>"./assets/post-id-4.png",
            "imgAlt"=>"Macbook aberto de frente para um IMac, as telas são de tamanhos diferentes",
            "link"=>"./blog-post.php?id=4"
        )
    );
?>