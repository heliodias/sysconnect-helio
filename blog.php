<?php
    $title = 'Blog | Sysconnect';
    include '_header.php';
?>
<section class="cabecalho cabecalho--blog">
    <div class="cabecalho__wrapper">
        <nav class="cabecalho__wrapper__breadcrumb">
            <ul>
                <li>Você está em</li>
                <li><a href="./">Home</a></li>
                <li>Blog</li>
            </ul>
        </nav>
        <div class="cabecalho__wrapper__title">
            <h1>Blog</h1>
        </div><!-- /TITLE -->
    </div> <!-- /WRAPPER -->
</section>
<section class="blog">
    <p class="subtitulo">Últimas notícias sobre</p>
    <h2>Tecnologia, Criação de Sites, Aplicativo e Sistemas</h2>
    <div class="blog__wrapper">
        <?php
         include_once "_blogPosts.php";
         foreach($postLista as $post):
        ?>
        <div class="cardPost">
            <img src="<?=$post['imgSrc']?>" alt="<?=$post['imgAlt']?>">
            <div class="cardPost__texto">
                <em><?=$post['data']?></em>
                <h3>
                    <a href="<?=$post['link']?>">
                        <?=$post['titulo']?>
                    </a>
                </h3>
                <a class="blogLer" href="<?=$post['link']?>">&gt; leia mais</a>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</section>
<section class="rodape rodape--blog">
        <div class="rodape__wrapper">
            <h2>
                <span>Buscando Transformar sua ideia em negócio?</span>
                Faça já seu orçamento Conosco
            </h2>
            <a href="./contato.php" class="btnPadrao btnPadrao--branco">Quero um orçamento gratuito</a>
        </div>
</section>
<?php
    include '_footer.php';
?>